#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

import mysql.connector

from facts.config import *


class DbCreate:
    """
    Create database and tables structure.
    """

    def __init__(self, dbauth):
        self.connect = dbauth

    def test_database(self):
        """
        Test if database is created
        """
        cursor = self.connect.create_cursor()
        cursor.execute(
            "SELECT SCHEMA_NAME "
            "FROM INFORMATION_SCHEMA.SCHEMATA "
            "WHERE SCHEMA_NAME = 'offdb'"
        )
        return cursor

    def create_database(self):
        """
        Create database offdb
        """
        cursor = self.connect.create_cursor()
        cursor.execute(
            "CREATE DATABASE IF NOT EXISTS offdb "
            "CHARACTER SET utf8mb4 "
            "COLLATE utf8mb4_unicode_ci"
        )

    def create_tables(self):
        """
        Create MySQL tables in offdb database.
        """
        cursor = self.connect.create_cursor()
        cursor.execute("USE `offdb`")

        cursor.execute(
            "CREATE TABLE IF NOT EXISTS `offdb`.`categories` ("
            "   `id` VARCHAR(80) NOT NULL,"
            "   `name` VARCHAR(80) NULL,"
            "   `url` VARCHAR(255) NULL,"
            "   `products` INT NULL,"
            "   PRIMARY KEY (`id`))"
            "   CHARACTER SET utf8mb4 "
            "   COLLATE utf8mb4_unicode_ci"
            "   ENGINE = InnoDB"
        )

        cursor.execute(
            "CREATE TABLE IF NOT EXISTS `offdb`.`products` ("
            "   `id` VARCHAR(80) NOT NULL,"
            "   `product_name` VARCHAR(80) NOT NULL,"
            "   `nutrition_grade_fr` CHAR(1) NOT NULL,"
            "   `brands` VARCHAR(80) NOT NULL,"
            "   `stores` VARCHAR(80) NULL,"
            "   `url` VARCHAR(255) NOT NULL,"
            "   `id_categorie` VARCHAR(60) NOT NULL,"
            "   PRIMARY KEY (`id`),"
            "   CONSTRAINT `fk_products_id_categorie`"
            "       FOREIGN KEY (`id_categorie`)"
            "       REFERENCES `categories` (`id`))"
            "   CHARACTER SET utf8mb4 "
            "   COLLATE utf8mb4_unicode_ci"
            "   ENGINE = InnoDB"
        )

        cursor.execute(
            "CREATE TABLE IF NOT EXISTS `offdb`.`preferences` ("
            "   `id_compared` VARCHAR(60) NOT NULL,"
            "   `id_result` VARCHAR(60) NOT NULL,"
            "   PRIMARY KEY (`id_compared`),"
            "   CONSTRAINT `fk_pref_id_compared`"
            "       FOREIGN KEY (`id_compared`)"
            "       REFERENCES `products` (`id`),"
            "   CONSTRAINT `fk_pref_id_result`"
            "       FOREIGN KEY (`id_result`)"
            "       REFERENCES `products` (`id`))"
            "   CHARACTER SET utf8mb4 "
            "   COLLATE utf8mb4_unicode_ci"
            "   ENGINE = InnoDB"
        )

    def drop(self):
        """
        Drop tables in offdb.
        """
        cursor = self.connect.create_cursor()
        queries = (
            ("SET foreign_key_checks = 0"),
            ("DROP DATABASE IF EXISTS `offdb`")
        )

        for query in queries:
            cursor.execute(query)
