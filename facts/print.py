#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

from facts.config import *

class Print:
    """
    Print results obtained from MySQL database.
    """
    @staticmethod
    def substitute(product):
        """
        View product with a more human-readable format.
        """
        print("Nom du produit ", "\t"*1, ":", product["name"])
        print("Score nutritionnel ", "\t"*1, ":", product["grade"])
        print("Marque ", "\t"*2, ":", product["brands"])
        print("Commerçants ", "\t"*2, ":", product["stores"])
        print("URL ", "\t"*3, ":", product["url"])
        print("\n")

    @staticmethod
    def result(cursor, name_list, id_list):
        """
        Print data contained in MySQL cursor.
        """
        for row in cursor:
            name_list.append(row[1])
            id_list.append(row[0])

        for idx, value in enumerate(name_list):
            print("  ", idx, "\t", ":", value)
