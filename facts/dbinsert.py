#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

import mysql.connector

from facts.config import *

class DbInsert:
    """
    Insert data in MySQL database.
    """

    def __init__(self, dbauth):
        self.connect = dbauth

    def insert_categories(self, data):
        """
        Insert parsed data from json.
        """
        cursor = self.connect.create_cursor()
        insert_query = "INSERT INTO categories (id, name, url, products) \
        VALUES (%(id)s, %(name)s, %(url)s, %(products)s)"
        try:
            cursor.executemany(insert_query, data)
        except mysql.connector.errors.IntegrityError:
            pass
        self.connect.commit()

    def insert_products(self, data):
        """
        Insert parsed data from json.
        """
        cursor = self.connect.create_cursor()
        insert_query = "INSERT INTO products \
        (id, product_name, nutrition_grade_fr, brands, stores, \
        url, id_categorie) \
        VALUES (%(id)s, %(product_name)s, %(nutrition_grade_fr)s, %(brands)s,\
        %(stores)s, %(url)s, %(categories_hierarchy)s) \
        ON DUPLICATE KEY UPDATE id = id"

        try:
            cursor.executemany(insert_query, data)
        except mysql.connector.errors.IntegrityError:
            pass

        self.connect.commit()

    def insert_substitute(self, compared, result):
        """
        Put id of the product to compare and the id of result
        product in database
        """
        cursor = self.connect.create_cursor()
        insert_query = (
            "INSERT INTO preferences (id_compared, id_result) \
            VALUES (%s, %s)"
        )
        values = (compared, result)

        try:
            cursor.execute(insert_query, values)
        except mysql.connector.errors.IntegrityError:
            print("Recherche déjà enregistrée.")

        self.connect.commit()
