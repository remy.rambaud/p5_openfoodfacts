#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

import mysql.connector
import facts

from facts.config import *

class DbRead:
    """
    Get and search data in MySQL database.
    """
    # pylint: disable=R0902

    def __init__(self, dbauth):
        self.connect = dbauth
        self.categories_list = []
        self.products_list = []
        self.id_categories_list = []
        self.id_products_list = []
        self.categorie_id = ""
        self.product_id = ""
        self.product_compared = {}
        self.product_result = {}
        self.pref_compared_name = []
        self.pref_compared_id = []
        self.pref_result_id = []

    def get_categories(self):
        """
        Get categories and send them to Print.result method.
        """
        query = (
            "SELECT categories.id, categories.name, COUNT(products.id) \
            FROM categories \
            INNER JOIN products ON products.id_categorie = categories.id \
            GROUP BY categories.id HAVING COUNT(products.id) > 1 \
            ORDER BY COUNT(products.id) DESC"
        )

        cursor = self.get_data(query)

        facts.print.Print.result(cursor, self.categories_list, self.id_categories_list)

    def get_products(self, index):
        """
        Send list of products according to user choice to Print.result method.
        """
        query = (
            "SELECT products.id, products.product_name \
            FROM products \
            WHERE products.id_categorie = %s \
            LIMIT 25"
        )
        value = (self.id_categories_list[index],)

        # Keep categorie id for subsequent test in get_substitute.
        self.categorie_id = self.id_categories_list[index]

        cursor = self.get_data(query, value)

        facts.print.Print.result(cursor, self.products_list, self.id_products_list)

    def get_substitute(self, index):
        """
        Compare the product chosen by the user and find another better rated
        product.
        """
        # Get nutrition grade
        query = (
            "SELECT product_name, nutrition_grade_fr, brands, stores, url, id \
            FROM products \
            WHERE products.id = %s"
        )
        value = (self.id_products_list[index],)

        cursor = self.get_data(query, value)

        product_compared = {}
        for name, grade, brands, stores, url, id in cursor:
            product_compared["name"] = name
            product_compared["grade"] = grade
            product_compared["brands"] = brands
            product_compared["stores"] = stores
            product_compared["url"] = url
            product_compared["id"] = id

        self.product_compared = product_compared

        # Find another product with better nutrition grade
        query = (
            "SELECT product_name, nutrition_grade_fr, brands, stores, url, id \
            FROM products \
            WHERE id_categorie = %s \
            AND products.nutrition_grade_fr < %s LIMIT 1"
        )
        value = (self.categorie_id, product_compared["grade"])

        cursor = self.get_data(query, value)

        product_result = {}
        for name, grade, brands, stores, url, id in cursor:
            product_result["name"] = name
            product_result["grade"] = grade
            product_result["brands"] = brands
            product_result["stores"] = stores
            product_result["url"] = url
            product_result["id"] = id

        self.product_result = product_result

        print("Produit sélectionné : \n")
        facts.print.Print.substitute(product_compared)

        if not product_result:
            print("Pas de meilleur produit trouvé. \n")
            return False

        print("Produit de remplacement : \n")
        facts.print.Print.substitute(product_result)
        return True

    def get_data(self, query, value=None):
        """
        Get data from database.
        """
        cursor = self.connect.create_cursor()
        cursor.execute("USE offdb")
        cursor.execute(query, value)
        return cursor

    def get_preferences(self):
        """
        Send a list of products already searched by the user.
        """
        query = (
            "SELECT product_name, id_compared, id_result \
            FROM preferences \
            INNER JOIN products ON preferences.id_compared = products.id"
        )
        cursor = self.get_data(query)

        for row in cursor:
            self.pref_compared_name.append(row[0])
            self.pref_compared_id.append(row[1])
            self.pref_result_id.append(row[2])

        for idx, value in enumerate(self.pref_compared_name):
            print("  ", idx, "\t", ":", value)

    def get_preferences_details(self, index):
        """
        Get details for each products.
        """
        query = (
            "SELECT product_name, nutrition_grade_fr, brands, stores, url, id \
            FROM products \
            WHERE products.id = %s"
        )
        value = (self.pref_compared_id[index],)
        cursor = self.get_data(query, value)

        product_compared = {}
        for name, grade, brands, stores, url, id in cursor:
            product_compared["name"] = name
            product_compared["grade"] = grade
            product_compared["brands"] = brands
            product_compared["stores"] = stores
            product_compared["url"] = url
            product_compared["id"] = id

        query = (
            "SELECT product_name, nutrition_grade_fr, brands, stores, url, id \
            FROM products \
            WHERE products.id = %s"
        )
        value = (self.pref_result_id[index],)
        cursor = self.get_data(query, value)

        product_result = {}
        for name, grade, brands, stores, url, id in cursor:
            product_result["name"] = name
            product_result["grade"] = grade
            product_result["brands"] = brands
            product_result["stores"] = stores
            product_result["url"] = url
            product_result["id"] = id

        print("Produit sélectionné : \n")
        facts.print.Print.substitute(product_compared)
        print("Produit de remplacement : \n")
        facts.print.Print.substitute(product_result)
