# Constants and configuration

URL_CAT = "https://fr.openfoodfacts.org/categories.json"
DIR_CAT = "data/"
FIL_CAT = "categories.json"

# Products by categories
PRODUCTS_NUMBER = 130

# Number of categories
CATEGORIES_NUMBER = 20

# Filters
KEYWORD_CAT = {"id", "name", "url", "products"}
KEYWORD_PROD = {"product_name", "url", "nutrition_grade_fr",
                "id", "brands", "stores", "categories_hierarchy"}

# MySQL authentication
MYSQL_HOST = "localhost"
MYSQL_USER = "off"
MYSQL_PASSWD = "pass"
MYSQL_DATABASE = "offdb"
