#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

import mysql.connector

from facts.config import *


class DbAuth:
    """
    Connect to MySQL server.
    """

    def __init__(self):
        self.host = MYSQL_HOST
        self.user = MYSQL_USER
        self.passwd = MYSQL_PASSWD
        self.sql_connect = None

    def connect(self):
        """
        Connect to MySQL server.
        """
        try:
            self.sql_connect = mysql.connector.connect(
                host=self.host,
                user=self.user,
                passwd=self.passwd
            )

        except mysql.connector.errors.InterfaceError:
            print("Serveur MySQL inaccessible.")
            exit()

    def create_cursor(self):
        """
        Cursor for MySQL.Connector
        """
        return self.sql_connect.cursor()

    def commit(self):
        """
        Autocommit not set : call commmit method
        """
        return self.sql_connect.commit()
