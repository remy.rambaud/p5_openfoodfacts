# Search a substitute with Open Food Facts

This application was conceived to help consumer in food shopping.
It is design to compare a product and to find a better one evaluated on its nutrition grade. All data are getted trought Open Food Facts API.

[Open Food Facts](https://fr.openfoodfacts.org/)

[Open Food Facts API](https://fr.openfoodfacts.org/data)

## How to

This program run with Python 3.5 and MySQL.

You need to create a MySQL user and give privileges to him on his database.
Credentials are placed in **config.py**.

You can do a first launch :

    python3 main.py --init

If the database is not created, initialization will begin automatically.

## General architecture

### Module Get

Its role is to download the data with Open Food Facts API and to parse them.

#### Class Json

* request() : Get JSON from Open Food Facts and write it in a file.
* request_url_list() : Create directory path and filename with given URL.

#### Class Parser

Read and filter JSON.

* path_constructor() : Create a list of file path, based on URL.
* parse_files() : Send a list of files given by **path_constructor()** to **parse_json()**.
* parse_json() :  Parse and apply filter on JSON.

### Module MySQL

This module manage MySQL database. It can perform tables creation, data insertion and read them.

#### Class DbAuth

Create authenticated object.

* connect() : Connect to MySQL server.
* create_cursor() : Create cursor for mysql.connector
* commit() : Commit inserts.

#### Class DbCreate

Create database and tables structure.

* create_database() : Create offdb database.
* create_tables() : Create all tables needed.
* drop() : Drop all tables.

#### Class DbInsert

* insert_categories() : Insert parsed categories in categories table.
* insert_products() : Insert parser products of each categories in products table.
* insert_substitute() : Insert found substitute.

#### Class DbRead

* get_categories() : Get the list of categories.
* get_products() : Get the list of products for the chosen categories.
* get_substitute() : Compare and get a substitute for a product based on its nutrition grade.
* get_data() : Used by all get_* methods to send a query to MySQL.
* get_preferences() : Get list of substitued products.
* get_preferences_details() : Get all informations availble on chosen subtitued product.

## Getting the database from Open Food Facts

Récupérer les *produits en français*.

### I. Download categories

    facts.get.Json.request()

The request() method need three arguments :

* directory     : path of directory
* data          : name of data file
* url           : targeted API URL

1. A json file is downloaded with given *URL*.
2. The destination of the *directory* is tested to ensure its existence.
3. A file is opened with write access on *data*.
4. The method json.dump is called with ensure_ascii=False to prevent conversion with incorrect charset.

### II. Parse categories.json

    facts.get.Parser.parse_json()

The parse_json() method need two arguments :

* data_input    : file containing json
* keyword_set   : set of keywords used as filter

1. The file is opened with read access, utf8 encoding is defined.
2. Data is stored in variable *data_json*.
3. This object is iterated in nested **for** loops to access wanted level of data.
4. Each products are tested with these criteria :
    * Products must have all wanted keywords.
    * All keywords must have a value.
5. A dictionnary is created with keywords and values found.
6. Each dictionnaries are appended to list *filtered_list*.
7. The method return the *filtered_list*.

### III. Parse products json

    # Expect as argument dictionnaries containing categories nested in a list.
    facts.get.Parser.path_constructor()

    # Expect as argument a list of URL.
    facts.get.Parser.request_url_list()

    # Expect as argument a list of files.
    facts.get.Parser.parse_files()

The method **path_constructor()** return a list of path files taken as argument by **request_url_list()**. This method download the number of categories defined in **config.py**

A second instance of **Parser()** is created to parse the product with the method **parse_file()**.
Each products have to match the filter defined in **config.py**.

### IV. User interface

In the main file, the interface is built with a set of nested **while** loops which keep open the program and wait for user input.

An instance of **DbAuth** is created for authentication in MySQL server with the **connect()** method.

    facts.mysql.DbAuth.connect()

On startup, the user is asked to input one of a the values specified in terminal :

* 1 : Afficher les catégories
* 2 : Afficher les produits substitués
* 0 : Quitter

#### Feature 1 : Afficher les catégories

Class **DbRead** can access to MySQL database. It expect as argument an object connected to MySQL database  :

    facts.mysql.DbRead(dbauth)

Retrieved data are sent to **Print** class which parse and display them :

    facts.mysql.Print()

1. An instance of **DbRead()** is created.
2. The method DbRead.**get_categories()** is called.
    * A query is sent to MySQL : All categories with more than one products are selected and sent to Print.**result()** method.
3. A list is built and printed in the terminal.
4. The user is asked to input one of the values corresponding to a category.
5. DbRead.**get_products(choice)** is called with the user's choice as argument.
    * A query is sent to MySQL : All products in the selected category are shown.
6. The user is asked to input one of the values associated to a product.
7. DbRead.**get_substitude(choice)** is called with the user's choice as argument.
    * A query is sent to MySQL : The nutrition grade of the selected product is compared to those of other products in database for the same category. The first product found is returned.
8. The method **insert_substitude()** of the class **DbInsert** is called with the **id** of the paired products as arguments. In this way, the two products are put in database.

#### Feature 2 : Afficher les produits substitués

This feature show all products already searched for which a substitute has been found.

    facts.mysql.DbRead.get_preferences()

1. The method **get_preferences()** of the class **DbRead** retrieve the list of paired products.
2. The user is asked to input one of the values associated to a product.
3. With user's choice as argument, the method **get_preferences_details(choice)** retrieve from database all informations about the paired products.

#### Feature 0 : Quitter

Call exit().