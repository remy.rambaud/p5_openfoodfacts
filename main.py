#! /usr/bin/env python3
# coding: utf-8

"""
Subtitute, with Open Food Facts database.
Find a product's substitute.
"""

import os
import sys

import facts
from facts.config import *

# Create instance for MySQL Connector authentication method and connect.
dbauth = facts.dbauth.DbAuth()
dbauth.connect()


def initialize():
    """
    For a first launch : download, parse and insert data in MySQL.
    """

    # Get json files from the OpenFactsFoods API and parse them.
    print("Get categories json.")
    get_json = facts.json.Json()
    get_json.request(DIR_CAT, FIL_CAT, URL_CAT)

    categories_parser = facts.parser.Parser()
    categories = categories_parser.parse_json(
        DIR_CAT + FIL_CAT,
        KEYWORD_CAT
    )
    categories = categories_parser.truncate(categories, "categories")

    print("Get products json.")
    get_json.request_url_list(categories)

    print("Parse products json. \n")
    files_list = categories_parser.path_constructor(categories)
    products_parser = facts.parser.Parser()
    products = products_parser.parse_files(files_list)
    products = products_parser.truncate(products, "products")

    print("Create database and tables.")
    dbstruc = facts.dbcreate.DbCreate(dbauth)
    dbstruc.drop()
    dbstruc.create_database()
    dbstruc.create_tables()

    print("Insert data in MySQL database. \n")
    insert_sql = facts.dbinsert.DbInsert(dbauth)
    insert_sql.insert_categories(categories)

    insert_sql = facts.dbinsert.DbInsert(dbauth)
    insert_sql.insert_products(products)


def clear():
    """
    Clear screen.
    """
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")


for arg in sys.argv:
    if arg == "--init":
        print("First start... \n")
        initialize()
        print("First start finished!")

def show_products():
    clear()
    in_loop = True
    while in_loop:
        print("Afficher les catégories")
        read = facts.dbread.DbRead(dbauth)
        read.get_categories()
        try:
            cat_choice = int(input("Choix : "))
            clear()
        except ValueError:
            clear()
            print("Valeur incorrecte.")
            break
        else:
            # Test if the user's choice is valid.
            if cat_choice <= len(read.categories_list) - 1:
                while in_loop:
                    print(
                        "Produits de la catégorie :",
                        "{}".format(
                            read.categories_list[cat_choice]),
                        "\n"
                    )
                    # The list is emptied to avoid adding
                    # unnecessary items.
                    read.products_list = []
                    read.get_products(cat_choice)
                    try:
                        prod_choice = int(input("Choix : "))
                        clear()
                    except ValueError:
                        clear()
                        print("Valeur incorrecte.")
                        break
                    else:
                        if prod_choice <= len(read.products_list) - 1:
                            print(
                                "Substitut pour le produit :",
                                "{}".format(
                                    read.products_list[prod_choice]),
                                "\n"
                            )
                            # If a substitute is found, the paired
                            # products are saved in database.
                            if read.get_substitute(prod_choice):
                                insert = facts.dbinsert.DbInsert(dbauth)
                                insert.insert_substitute(
                                    read.product_compared["id"],
                                    read.product_result["id"]
                                )
                            in_loop = False
                in_loop = False
    return in_loop

def show_subtitute():
    in_loop = True
    while in_loop:
        print("Liste des recherches effectuées :")
        read = facts.dbread.DbRead(dbauth)
        read.get_preferences()
        try:
            prod_choice = int(input("Choix : "))
            clear()
        except ValueError:
            clear()
            print("Valeur incorrecte.")
            break
        else:
            if read.pref_compared_name and \
                    prod_choice <= len(read.pref_compared_name) - 1:
                print(
                    "Substitut pour le produit :",
                    "{}".format(
                        read.pref_compared_name[prod_choice]), "\n"
                )
                read.get_preferences_details(prod_choice)
                in_loop = False
            else:
                continue

def __main__():

    in_loop = True
    while in_loop:
        try:
            print(
                "\n" + "1 : Afficher les catégories"
                "\n" + "2 : Afficher les produits substitués"
                "\n" + "0 : Quitter"
            )
            main_choice = int(input("Choix : "))
            clear()
        # Print an error if the user's choice isn't valid.
        except ValueError:
            clear()
            print("Valeurs acceptées : 0 1 2")
        else:

            if main_choice == 1:
                show_products()
            elif main_choice == 2:
                show_subtitute()
            elif main_choice == 0:
                exit()
            else:
                print("Choix incorrect")


# Test if database is created.
dbtest = facts.dbcreate.DbCreate(dbauth)
offdb = dbtest.test_database()
if not offdb.fetchone() == (MYSQL_DATABASE,):
    initialize()

if __name__ == "__main__":
    __main__()
